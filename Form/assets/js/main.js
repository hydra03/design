$(document).ready(function(){
  //Sample Form Data
  var sampleData = {
    title : 'Form Title',
    description : 'Form description',
    fields : [
      {
        elem: 'input',
        label: 'Age',
        type: 'number',
        required: true
      },

      {
        elem: 'select',
        label: 'Gender',
        options: [
          {
            value: 'Male',
            checked: true
          },
          {
            value: 'Female',
            checked: false
          },
          {
            value: 'Harold',
            checked: false
          }
        ],
        required: true
      },

      {
        elem: 'radio',
        label: 'Salary range',
        options: [
          {
            value: '999,999 - 500,000',
            checked: true
          },
          {
            value: '499,999 - 100,000',
            checked: false
          }
        ],
        required: false
      },

      {
        elem: 'checkbox',
        label: 'Vegetables available',
        options: [
          {
            value: 'Carrot',
            checked: true
          },
          {
            value: 'Asparagus',
            checked: true
          }
        ],
        required: true
      }
    ]
  }


  //Class
  var formControl = function(){
    var lib = {};
    var priv = {};

    //Append Form Panel
    priv.renderPanel = function(data){
      var template;

      // if(data['elem'] == 'input'){template = $('#input-template').html();}
      // else if(data['elem'] == 'textarea'){template = $('#textarea-template').html();}
      // else if(data['elem'] == 'select'){template = $('#select-template').html();}
      // else if(data['elem'] == 'radio'){template = $('#radio-template').html();}
      // else{template = $('#checkbox-template').html();}

      template = $('#'+data['elem']+'-template').html();   

      if(data['required']==true){ data['required'] = 'checked';}
      else{data['required'] = '';}

      //if(data['elem'] == 'select'){$()}

      var rendered = Mustache.render(template, data);
      $('#form-panels').append(rendered);
    }

    //Append Form radio, option or checkbox
    priv.renderORC = function(panel, data){
      var template;

      if(panel['elem'] == 'select'){template = $('#options-select-template').html();}
      else if(panel['elem'] == 'radio'){template = $('#options-radio-template').html();}
      else if(panel['elem'] == 'checkbox'){template = $('#options-checkbox-template').html();}

      //console.log(data);
      var rendered = Mustache.render(template, data);
      $('[panel-id="'+panel['panel-index']+'"] .options-group').append(rendered);

      //ADD ORC INDEX HERE CODE
    }

    //Add form panel
    lib.addFieldPanel =function(el){
      //priv.renderPanel(template, panel);

      var panel = [];
      var shit;

      panel['elem'] = $(el).attr('itemtype');
      panel['panel-index'] = sampleData.fields.length; //dapat plus 1 pa; temp code

      shit = {elem: panel['elem'], label: '', options: [], required: true};
      sampleData.fields.push(shit);

      priv.renderPanel(panel);
      console.log('sampleData: '+ JSON.stringify(sampleData));
    }

    //Add form options, radio, checkbox
    lib.addFieldORC = function(el){
      //priv.renderORC(panel, template, data);
      var panel = [];
      var data = [];
      var orc;

      panel['panel-index'] = $(el).closest('.field-panel-item').attr('panel-id');//Gets the panel-index
      panel['elem'] = sampleData.fields[panel['panel-index']].elem; //Gets the type of the input to be passed
      data['orc-index'] = sampleData.fields[panel['panel-index']].options.length;

      if(panel['elem'] == 'select'){orc = {value: ''}}
      else{orc = {value: '', checked: false}}
      
      sampleData.fields[panel['panel-index']].options.push(orc);
      
      priv.renderORC(panel, data);
    }

    //Remove panel
    lib.deleteFieldPanel = function(el){
      var panel = [];

      panel['panel-index'] = $(el).closest('.field-panel-item').attr('panel-id');
      sampleData.fields.splice(panel['panel-index'], 1);
      $(el).closest('.field-panel-item').remove();

      console.log('sampleData: '+ JSON.stringify(sampleData));
    }

    //Remove form options, radio, checkboxl
    lib.deleteFieldORC = function(el){
      var panel = [];
      panel['panel-index'] = $(el).closest('.field-panel-item').attr('panel-id');
      panel['orc-index'] = $(el).closest('.form-group').attr('orc-id');

      sampleData.fields[panel['panel-index']].options.splice([panel['orc-index']], 1);
      
      //sampleData.fields[panel['panel-index']].options[panel['orc-index']].value;
      
      $(el).closest('.form-group').remove();
    }

    //Loads and Generate the field from JSON Data
    lib.generateFields = function(form){
      $('#form-title').val(form.title);
      $('#form-description').val(form.description);

      //each form panel
      $.each(form.fields, function(j, e){
        e['panel-index'] = j;
        priv.renderPanel(e);
        
        //Only for certain panel/Input type
        if(e.elem == 'select' || e.elem == 'radio' || e.elem == 'checkbox'){
          $.each(e.options, function(k, f){
            f['orc-index'] = k;
            priv.renderORC(e, f);
          });
        }
      });
    }

    lib.editLabel = function(el){
      var panel = [];
      var temp = {};

      panel['panel-index'] = $(el).closest('.field-panel-item').attr('panel-id');//Gets the panel-index
      panel['label'] = $(el).closest('.field-panel-item').find('.editlabel').html();

      sampleData.fields[panel['panel-index']].label = panel['label'];
    }

    lib.editFormInfo = function(data){
      if(data['elem']=='title'){sampleData.title = data['value'];}
      else if(data['elem']=='description'){sampleData.description = data['value'];}
      console.log(JSON.stringify(sampleData));
    }


    lib.modifyInputBar = function(el){
      var panel = [];
      var temp = {};

      panel['panel-index'] = $(el).closest('.field-panel-item').attr('panel-id');//Gets the panel-index
      panel['input-type'] = $(el).closest('.field-panel-item').find('.type-selector').val();

      $('[panel-id="'+panel['panel-index']+'"] input.form-control').attr('type', panel['input-type']);
      sampleData.fields[panel['panel-index']].type = panel['input-type'];
    }
    
    lib.modifyORC = function(el){
      var panel = [];

      panel['panel-index'] = $(el).closest('.field-panel-item').attr('panel-id');//Gets the panel-index
      panel['orc-index'] = $(el).closest('.form-group').attr('orc-id');

      sampleData.fields[panel['panel-index']].options[panel['orc-index']].value = $(el).val();
    }

    lib.formCheck = function(formname){      
      var errors = 0;

      $(formname).find('input[required]').each(function(){
          if(!$(this).val()){
              $(this).closest('.form-group').addClass('has-error');
              errors++;
              //$('.error-message').show();
          }
          else{
              $(this).removeClass('input-error');
          }
      });

      $(formname).find('[contenteditable="true"][required]').each(function(){
          if(!$(this).html()){
              $(this).addClass('alert-danger');
              errors++;
              //$('.error-message').show();
          }
          else{
              $(this).removeClass('alert-danger');
          }
      });

      if(errors < 1)
          return true;
      else
          return false;
    }

    

    return lib;
  }

  
  //Create instance
  FormControl = new formControl();
  FormControl.generateFields(sampleData);

  //Init
  $('.error-message').hide();

  //Add Item button to create Form Panel
  $('.additem-btn').on('click',function(e){
    e.preventDefault();
    
    FormControl.addFieldPanel(this);
  });

  //Delete per Form Panel
  $(document).on('click','.remove-panel-btn',function(e){
    e.preventDefault();
    FormControl.deleteFieldPanel(this);
  });

  //Add Option, Radio, Checkbox
  $(document).on('click', '.add-orc-btn',function(e){
    e.preventDefault();
    FormControl.addFieldORC(this);
  });

  $(document).on('click','.remove-orc-btn',function(e){
    e.preventDefault();
    FormControl.deleteFieldORC(this);
  });

  $(document).on('change', '.type-selector', function(e){
    e.preventDefault();
    console.log('change');
    FormControl.modifyInputBar(this);
  });
  
  $(document).on('input', '.editlabel', function(e){
    e.preventDefault();
    console.log('change');
    FormControl.editLabel(this);
  });

  $(document).on('input', '#form-title', function(e){
    var data = [];
    data['elem'] = 'title';
    data['value'] = $(this).val();
    FormControl.editFieldInfo(data);
  });

  $(document).on('input', '#form-description', function(e){
    var data = [];
    data['elem'] = 'description';
    data['value'] = $(this).val();
    FormControl.editFormInfo(data);
  });

  $('.save-btn').on('click', function(){
    FormControl.formCheck('#panel-create');
  });

  $(document).keypress(function(e){
    var key = e.which;
    if(key == 13)  // the enter key code
    {
      console.log(JSON.stringify(sampleData)); 
    }
        
  });

  $(document).on('input','.orc-input', function(e){
    e.preventDefault();
    FormControl.modifyORC(this);
  });

  

});
