$(document).ready(function() {

/*-----Lister Class-----*/	

	var Lister = function(){

		var lib ={};
		var myList = [];
	

		lib.display = function(data){
			$('ul').html('');

			$.each(myList, function(index, value){
				$('ul').append('<li>' +value+ '</li>');

			});
	
		}

		lib.addItem = function(data){
			myList.push(data);

		}

		lib.popItem = function(data){
			myList.pop();

		}

		return lib;

	}

/*-----Panel Builder Class-----*/

	var PanelBuilder = function(data){

		var pan = {};

		var myArr = [{value1: "qwerty", value2: "12345"}, {value1: "uiop", value2: "78945"}];

		var code = '<div class="panel panel-default panel-field" style="margin-top: 30px;" panel-id="">'+
				   '<div class="panel-body">'+
              	   '<form>'+
                   '<input type="text" class="form-control invalue1 input-box" placeholder="Input1">'+
                   '<input type="text" class="form-control invalue2 input-box" placeholder="Input2" style="margin-top: 10px;">'+
                   '</form>'+
                   '<button id="delete-btn" class="btn btn-default" type="button" style="margin-top: 10px">Delete</button>'+
            	   '</div>';

		for(var i = 0; i < myArr.length; i++){
		
	    var code2 = '<div class="panel panel-default panel-field" style="margin-top: 30px;" panel-id="'+i+'">'+
				    '<div class="panel-body">'+
              	    '<form>'+
                    '<input type="text" class="form-control invalue1 input-box" placeholder="'+myArr[i]['value1']+'">'+
                    '<input type="text" class="form-control invalue2 input-box" placeholder="'+myArr[i]['value2']+'" style="margin-top: 10px;">'+
                    '</form>'+
                    '<button id="delete-btn" class="btn btn-default" type="button" style="margin-top: 10px">Delete</button>'+
            	    '</div>';

         $('.pbody').append(code2);

        }
		pan.add = function(data){

			var myData = {value1: "", value2: ""};

			$('.pbody').append(code);
			var iLength = myArr.length;

        	$('.pbody .panel:last-child').attr('panel-id', iLength);

        	myArr.push(myData);
        	console.log(JSON.stringify(myArr));

		}

		pan.save = function(data){
			$('.input-box').each(function(){
				var m = $(this).closest('.panel').attr('panel-id');

				console.log('[panel-id="'+m+'"]');

				myArr[i].value1 = $('[panel-id="'+m+'"] .input-box.invalue1').val();
				myArr[i].value2 = $('[panel-id="'+m+'"] .input-box.invalue2').val();
				console.log(JSON.stringify(myArr));
			});
		}

		pan.delete = function(data){

			// console.log(data);
			var del = $(data).closest('.panel').attr('panel-id');
			console.log('panelid ----> '+del);

			$('[panel-id="'+del+'"]').remove();
			myArr.splice(del, 1);

			var j = 0;
			
			$('.panel-field').each(function(){
				$(this).attr('panel-id', j);
				j++;

			});

		}

		pan.generate = function(data){

			$.each(myArr, function(index, value){

				console.log(JSON.stringify(myArr));

			});

		}

		return pan;
	}

/*-----Instances-----*/

	Controller = new Lister();

	Paneller = new PanelBuilder();

/*-----Lister Handlers-----*/

	Paneller.generate();

		$(document).on('click', '#display-btn', function(e){
			e.preventDefault();

			var inputvalue = $('.textinput').val();

			Controller.addItem(inputvalue);
			Controller.display();
			
		});

		$(document).on('click', '#pop-btn', function(e){
			e.preventDefault();

			Controller.popItem();
			Controller.display();
			
		});

		$(document).on('click', '#red-btn', function(e){
			e.preventDefault();

			$('.boxed').removeClass('blue').addClass('red');

			
		});

		$(document).on('click', '#blue-btn', function(e){
			e.preventDefault();

			$('.boxed').removeClass('red').addClass('blue');
		});

		$(document).on('click', '#clear-btn', function(e){
			e.preventDefault();

			$('.boxed').removeClass('red').removeClass('blue');
		});

/*-----Panel Builder Handlers-----*/

		$(document).on('click', '#add-panel', function(e){
			e.preventDefault();

			Paneller.add();
		});

		$(document).on('click', '#save-panel', function(e){
			e.preventDefault();

			console.log('saved');

			Paneller.save();

		});

		$(document).on('click', '#delete-btn', function(e){
			e.preventDefault();

			console.log('deleted');

			Paneller.delete(this);
		});

});

