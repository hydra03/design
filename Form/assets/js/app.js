$(document).ready(function(){


  var formControl = function(){
    var lib = {};
    var priv = {};

    priv.privatefunction =function(){

    }

    lib.createForm = function(){
      //code
    }

    lib.generateForm = function(form){
      
    }

    lib.addInput = function(data){
      var html = 'input added';

      $('#addbtn-wrapper').prepend(html);
    } 

    lib.addcheckbox = function(data){
      var html = '<div class="form-group">'+
                    '<div class="input-group">'+
                       '<span class="input-group-addon">'+
                         '<input type="checkbox" aria-label="...">'+
                       '</span>'+
                       '<input type="text" class="form-control" aria-label="...">'+
                       '<span class="input-group-btn">'+
                        '<button class="btn btn-default btn-transparent remove-checkbox-btn" type="button"><span class="fa fa-close text-danger"></span></button>'+
                      '</span>'+
                    '</div>'+
                 '</div>';

      $('.checkbox-options').append(html);
    }

    lib.selectOptions = function(data){
      var html = '<div class="form-group">'+
                 '<div class="input-group">'+
                 '<input type="text" class="form-control" aria-label="...">'+
                 '<span class="input-group-btn">'+
                 '<button class="btn btn-default btn-transparent remove-select-btn" type="button"><span class="fa fa-close text-danger"></span></button>'+
                 '</span>'+
                 '</div>'+ 
                 '</div>';

      $('.select-options').append(html);
    }

    lib.deleteSelectOption = function(data){
      $(data).closest('.form-group').remove();
    }

    lib.removecheckbox = function(data){
      
      $(data).closest('.form-group').remove();
    }

    lib.removepanel = function(data){
      $(data).closest('.panel').remove();
    }

    lib.addpanel =function(data){
     
      if(data['type'] == 'input'){
        var html =
        '<div class="row">'+
                '<div class="col-md-12 col-md-offset-0">'+
                  '<div class="panel panel-default item-panel shadow-style-1 border-top-style-1" id="panel-input">'+
                    '<div class="panel-body">'+
                      '<div class="row">'+
                        '<div class="col-md-12">'+
                          '<div class="btn-group pull-right">'+
                            '<button type="button" class="btn btn-primary btn-xs"><i class="fa fa-copy" aria-hidden="true"></i> Copy</button>'+
                            '<button type="button" class="btn btn-primary btn-xs remove-panel-btn"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>'+
                            '<button type="button" class="btn btn-primary btn-xs"><i class="fa fa-long-arrow-up" aria-hidden="true"></i></button>'+
                            '<button type="button" class="btn btn-primary btn-xs"><i class="fa fa-long-arrow-down" aria-hidden="true"></i></button>'+
                          '</div>'+
                        '</div>'+
                      '</div>'+
                      '<div class="row">'+
                        '<div class="col-md-12">'+
                          '<div class="form-group">'+
                            '<label class="editlabel" contenteditable="true" place-text="Editable Label"></label>'+
                            '<input type="text" class="form-control" placeholder="">'+
                          '</div>'+
                        '</div>'+
                      '</div>'+
                      '<div class="row">'+
                        '<div class="col-md-6">'+
                          
                          '<form class="form-inline">'+
                            '<div class="form-group">'+
                              '<label class="text-thin">Input type</label>'+
                              '<select id="sorter" class="form-control">'+
                                '<option class="select-opt"><a href="#"> Text</a></option>'+
                                '<option class="select-opt"><a href="#"> Number</a></option>'+
                                '<option class="select-opt"><a href="#"> Email</a></option>'+
                                '<option class="select-opt"><a href="#"> Password</a></option>'+
                              '</select>'+
                            '</div>'+
                          '</form>'+

                            
                        '</div>'+
                        '<div class="col-md-3 col-md-offset-3">'+
                           '<div class="checkbox pull-right">'+
                              '<label>'+
                                '<input id="slider" type="checkbox" checked data-toggle="toggle">&nbsp;Required'+
                              '</label>'+
                           '</div>'+
                        '</div>'+
                     '</div>'+
                    '</div>'+
                  '</div>'+
                '</div>'+
              '</div>'
              '</div>';
      //$('.panel').append(html);
        $('.inserthere').append(html);
        console.log('input yes');
      } 

      else if(data['type'] == 'textarea'){
        var html = '<div class="row">'+
                    '<div class="col-md-12 col-md-offset-0">'+
                      '<div class="panel panel-default item-panel shadow-style-1 border-top-style-1" id="panel-textarea">'+
                        '<div class="panel-body">'+
                          '<div class="row">'+
                            '<div class="col-md-12">'+
                              '<div class="btn-group pull-right">'+
                                '<button type="button" class="btn btn-primary btn-xs"><i class="fa fa-copy" aria-hidden="true"></i> Copy</button>'+
                                '<button type="button" class="btn btn-primary btn-xs remove-panel-btn"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>'+
                                '<button type="button" class="btn btn-primary btn-xs"><i class="fa fa-long-arrow-up" aria-hidden="true"></i></button>'+
                                '<button type="button" class="btn btn-primary btn-xs"><i class="fa fa-long-arrow-down" aria-hidden="true"></i></button>'+
                              '</div>'+
                            '</div>'+
                          '</div>'+
                          '<div class="row">'+
                            '<div class="col-md-12">'+
                              '<label class="editlabel" contenteditable="true" place-text="Editable Label"></label>'+
                              '<textarea class="form-control" rows="5" placeholder="Text here..."></textarea>'+
                            '</div>'+
                          '</div>'+
                          '<div class="row">'+
                            '<div class="col-md-12">'+
                              '<div class="pull-right">'+
                                '<div class="checkbox">'+
                                  '<label>'+
                                    '<input id="slider" type="checkbox" checked data-toggle="toggle"> Required'+
                                  '</label>'+
                                '</div>'+
                              '</div>'+
                            '</div>'+
                          '</div>'+
                        '</div>'+
                      '</div>'+
                    '</div>'+
                  '</div>';
        console.log('textarea');
        $('.inserthere').append(html);
      }

      else if(data['type'] == 'select'){
        var html = '<div class="row">'+
                    '<div class="col-md-12 col-md-offset-0">'+
                      '<div class="panel panel-default item-panel shadow-style-1 border-top-style-1" id="panel-select">'+
                        '<div class="panel-body">'+
                          '<div class="row">'+
                            '<div class="col-md-12">'+
                              '<div class="btn-group pull-right">'+
                                '<button type="button" class="btn btn-primary btn-xs"><i class="fa fa-copy" aria-hidden="true"></i> Copy</button>'+
                                '<button type="button" class="btn btn-primary btn-xs remove-panel-btn"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>'+
                                '<button type="button" class="btn btn-primary btn-xs"><i class="fa fa-long-arrow-up" aria-hidden="true"></i></button>'+
                                '<button type="button" class="btn btn-primary btn-xs"><i class="fa fa-long-arrow-down" aria-hidden="true"></i></button>'+
                              '</div>'+
                            '</div>'+
                          '</div>'+
                          '<div class="row">'+
                            '<div class="col-md-12">'+
                              '<div class="form-group">'+
                                '<label class="editlabel" contenteditable="true" place-text="Editable Label"></label>'+
                                '<select id="sorter" class="form-control">'+
                                  '<option class="select-opt"><a href="#"> Choices</a></option>'+
                                  '<option class="select-opt"><a href="#"> Date</a></option>'+
                                  '<option class="select-opt"><a href="#"> Time</a></option>'+
                                  '<option class="select-opt"><a href="#"> Type</a></option>'+
                                '</select>'+
                              '</div>'+
                            '</div>'+
                          '</div>'+
                          '<div class="row">'+
                              '<div class="col-md-6">'+
                                '<div class="select-options">'+
                                   '<div class="form-group">'+
                                      '<div class="input-group">'+
                                         '<input type="text" class="form-control" aria-label="...">'+
                                         '<span class="input-group-btn">'+
                                          '<button class="btn btn-default btn-transparent remove-select-btn" type="button"><span class="fa fa-close text-danger"></span></button>'+
                                        '</span>'+
                                      '</div><!-- /input-group -->'+
                                   '</div>'+
                                   '<div class="form-group">'+
                                      '<div class="input-group">'+
                                         '<input type="text" class="form-control" aria-label="...">'+
                                         '<span class="input-group-btn">'+
                                          '<button class="btn btn-default btn-transparent remove-select-btn" type="button"><span class="fa fa-close text-danger"></span></button>'+
                                          '</span>'+
                                      '</div><!-- /input-group -->'+
                                   '</div>'+
                                   '<div class="form-group">'+
                                      '<div class="input-group">'+
                                        '<input type="text" class="form-control" aria-label="...">'+
                                        '<span class="input-group-btn">'+
                                          '<button class="btn btn-default btn-transparent remove-select-btn" type="button"><span class="fa fa-close text-danger"></span></button>'+
                                        '</span>'+
                                      '</div><!-- /input-group -->'+
                                   '</div>'+
                              '</div>'+
                              '</div>'+
                          '</div>'+
                          '<div class="row">'+
                            '<div class="col-md-3">'+
                               '<div class="btn-group">'+
                                  '<button type="button" class="btn btn-primary btn-md add-option-btn">+ Add Option</button>'+
                               '</div>'+
                            '</div>'+
                            '<div class="col-md-3 col-md-offset-6">'+
                               '<div class="checkbox pull-right">'+
                                  '<label>'+
                                    '<input id="slider" type="checkbox" checked data-toggle="toggle">&nbsp;Required'+
                                  '</label>'+
                               '</div>'+
                            '</div>'+
                          '</div>'+
                        '</div>'+
                      '</div>'+
                    '</div>'+
                  '</div>';
        console.log('select');
        $('.inserthere').append(html);
      }
      else if(data['type'] == 'radio'){
        var html = '<div class="radio-panel">'+
                          '<div class="row">'+
                            '<div class="col-md-12 col-md-offset-0">'+
                              '<div class="panel panel-default item-panel shadow-style-1 border-top-style-1" id="panel-radio">'+
                                '<div class="panel-body">'+
                                  
                                    '<div class="btn-group pull-right">'+
                                      '<button type="button" class="btn btn-primary btn-xs"><i class="fa fa-copy" aria-hidden="true"></i> Copy</button>'+
                                      '<button type="button" class="btn btn-primary btn-xs remove-panel-btn"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>'+
                                      '<button type="button" class="btn btn-primary btn-xs"><i class="fa fa-long-arrow-up" aria-hidden="true"></i></button>'+
                                      '<button type="button" class="btn btn-primary btn-xs"><i class="fa fa-long-arrow-down" aria-hidden="true"></i></button>'+
                                    '</div>'+
                                    
                                   '<div class="row">'+
                                      '<div class="col-md-12">'+
                                         '<div class="form-group">'+
                                            '<label>&nbsp; </label>'+
                                            '<input type="text" class="form-control" placeholder="Question">'+
                                         '</div>'+
                                      '</div>'+
                                   '</div>'+
                                   
                                   '<div class="row">'+
                                      '<div class="col-md-6">'+

                                        '<div class="radio-options">'+
                                           '<div class="form-group">'+
                                              '<div class="input-group">'+
                                                 '<span class="input-group-addon">'+
                                                   '<input type="radio" name="option-1" aria-label="...">'+
                                                 '</span>'+
                                                 '<input type="text" class="form-control" aria-label="...">'+
                                                 '<span class="input-group-btn">'+
                                                  '<button class="btn btn-default btn-transparent remove-radio-btn" type="button"><span class="fa fa-close text-danger "></span></button>'+
                                                '</span>'+
                                              '</div><!-- /input-group -->'+
                                           '</div>'+

                                           '<div class="form-group">'+
                                              '<div class="input-group">'+
                                                 '<span class="input-group-addon">'+
                                                   '<input type="radio" name="option-1" aria-label="...">'+
                                                 '</span>'+
                                                 '<input type="text" class="form-control" aria-label="...">'+
                                                 '<span class="input-group-btn">'+
                                                  '<button class="btn btn-default btn-transparent remove-radio-btn" type="button"><span class="fa fa-close text-danger "></span></button>'+
                                                  '</span>'+
                                              '</div><!-- /input-group -->'+
                                           '</div>'+

                                           '<div class="form-group">'+
                                              '<div class="input-group">'+
                                                '<span class="input-group-addon">'+
                                                  '<input type="radio" name="option-1" aria-label="...">'+
                                                '</span>'+
                                                '<input type="text" class="form-control" aria-label="...">'+
                                                '<span class="input-group-btn">'+
                                                  '<button class="btn btn-default btn-transparent remove-radio-btn" type="button"><span class="fa fa-close text-danger "></span></button>'+
                                                '</span>'+
                                              '</div><!-- /input-group -->'+
                                           '</div>'+
                                        '</div>'+ 


                                      '</div>'+
                                   '</div>'+
                                  
                                   '<div class="row">'+
                                      '<div class="col-md-3">'+
                                         '<div class="btn-group">'+
                                            '<button type="button" class="btn btn-primary btn-md add-radio-btn">+ Add Radio Button</button>'+
                                         '</div>'+
                                      '</div>'+
                                      '<div class="col-md-3 col-md-offset-6">'+
                                         '<div class="checkbox pull-right">'+
                                            '<label>'+
                                              '<input id="slider" type="checkbox" checked data-toggle="toggle">&nbsp;Required'+
                                            '</label>'+
                                         '</div>'+
                                      '</div>'+
                                   '</div>'+

                                '</div>'+
                              '</div>'+
                            '</div>'+
                          '</div>'+
                        '</div>';

              console.log('radio');
      $('.inserthere').append(html);                


      }
      else{
        var html = '<div class="row">'+
                          '<div class="col-md-12 col-md-offset-0">'+
                            '<div class="panel panel-default item-panel shadow-style-1 border-top-style-1" id="panel-checkbox">'+
                              '<div class="panel-body">'+
                                
                                  '<div class="btn-group pull-right">'+
                                    '<button type="button" class="btn btn-primary btn-xs"><i class="fa fa-copy" aria-hidden="true"></i> Copy</button>'+
                                    '<button type="button" class="btn btn-primary btn-xs remove-panel-btn"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>'+
                                    '<button type="button" class="btn btn-primary btn-xs"><i class="fa fa-long-arrow-up" aria-hidden="true"></i></button>'+
                                    '<button type="button" class="btn btn-primary btn-xs"><i class="fa fa-long-arrow-down" aria-hidden="true"></i></button>'+
                                  '</div>'+
                                  
                                 '<div class="row">'+
                                    '<div class="col-md-12">'+
                                       '<div class="form-group">'+
                                          '<label>&nbsp; </label>'+
                                          '<input type="text" class="form-control" placeholder="Question">'+
                                       '</div>'+
                                    '</div>'+
                                 '</div>'+
                                 
                                 '<div class="row">'+
                                    '<div class="col-md-6">'+
                                      '<div class="checkbox-options" >'+
                                         '<div class="form-group">'+
                                            '<div class="input-group">'+
                                               '<span class="input-group-addon">'+
                                                 '<input type="checkbox" aria-label="...">'+
                                               '</span>'+
                                               '<input type="text" class="form-control" aria-label="...">'+
                                               '<span class="input-group-btn">'+
                                                '<button class="btn btn-default btn-transparent remove-checkbox-btn" type="button"><span class="fa fa-close text-danger"></span></button>'+
                                              '</span>'+
                                            '</div><!-- /input-group -->'+
                                         '</div>'+
                                         '<div class="form-group">'+
                                            '<div class="input-group">'+
                                               '<span class="input-group-addon">'+
                                                 '<input type="checkbox" aria-label="...">'+
                                               '</span>'+
                                               '<input type="text" class="form-control" aria-label="...">'+
                                               '<span class="input-group-btn">'+
                                                '<button class="btn btn-default btn-transparent remove-checkbox-btn" type="button"><span class="fa fa-close text-danger"></span></button>'+
                                                '</span>'+
                                            '</div><!-- /input-group -->'+
                                         '</div>'+
                                         '<div class="form-group">'+
                                            '<div class="input-group">'+
                                              '<span class="input-group-addon">'+
                                                '<input type="checkbox" aria-label="...">'+
                                              '</span>'+
                                              '<input type="text" class="form-control" aria-label="...">'+
                                              '<span class="input-group-btn">'+
                                                '<button class="btn btn-default btn-transparent remove-checkbox-btn" type="button"><span class="fa fa-close text-danger"></span></button>'+
                                              '</span>'+
                                            '</div><!-- /input-group -->'+
                                         '</div>'+
                                      '</div>'+
                                    '</div>'+
                                 '</div>'+
                                
                                 '<div class="row" id="checkbox-opt">'+
                                    '<div class="col-md-3">'+
                                        '<button type="button" class="btn btn-primary btn-md add-checkbox-btn">+ Add Checkbox</button>'+
                                    '</div>'+
                                    '<div class="col-md-3 col-md-offset-6">'+
                                       '<div class="checkbox pull-right">'+
                                          '<label>'+
                                            '<input id="slider" type="checkbox" checked data-toggle="toggle">&nbsp;Required'+
                                          '</label>'+
                                       '</div>'+
                                    '</div>'+
                                 '</div>'+

                              '</div>'+
                            '</div>'+
                          '</div>'+
                        '</div>';
        console.log('checkbox');
        $('.inserthere').append(html);
      }
    
    }

    lib.addRadio = function(data){
      var html = '<div class="form-group">'+
                  '<div class="input-group">'+
                  '<span class="input-group-addon">'+
                  '<input type="radio" name="option-1" aria-label="...">'+
                  '</span>'+
                  '<input type="text" class="form-control" aria-label="...">'+
                  '<span class="input-group-btn">'+
                  '<button class="btn btn-default btn-transparent remove-radio-btn" type="button"><span class="fa fa-close text-danger"></span></button>'+
                  '</span>'+
                  '</div>'+
                  '</div>';

      $('.radio-options').append(html);
    } 

    lib.deleteRadio = function(data){

      $(data).closest('.form-group').remove();
    }
    


    lib.addRadio = function(data){
      var html = '<div class="form-group">'+
                  '<div class="input-group">'+
                  '<span class="input-group-addon">'+
                  '<input type="radio" name="option-1" aria-label="...">'+
                  '</span>'+
                  '<input type="text" class="form-control" aria-label="...">'+
                  '<span class="input-group-btn">'+
                  '<button class="btn btn-default btn-transparent remove-radio-btn" type="button"><span class="fa fa-close text-danger"></span></button>'+
                  '</span>'+
                  '</div>'+
                  '</div>';

      $('.radio-options').append(html);
    } 

    lib.deleteRadio = function(data){

      $(data).closest('.form-group').remove();
    }


    return lib;
  }
  
  //Create instance
  FormControl = new formControl();

  //Sample event handler
  $(document).on('click','#add-input-input', function(e){
    e.preventDefault();

    FormControl.addInput();
  });

  $(document).on('click','.add-checkbox-btn',function(e){
    e.preventDefault();
    console.log('click');
    FormControl.addcheckbox();
  });

  $(document).on('click','.add-option-btn', function(e){
    e.preventDefault();
    // console.log('click');

    FormControl.selectOptions();
  });

  $(document).on('click','.remove-checkbox-btn',function(e){
    e.preventDefault();
    console.log('click');
    FormControl.removecheckbox(this);
  });

  $(document).on('click','.remove-select-btn', function(e){
    e.preventDefault();

    var sel = $(this);
    FormControl.deleteSelectOption(sel);
  });

  $(document).on('click','.remove-panel-btn',function(e){
    e.preventDefault();
    console.log('click');
    FormControl.removepanel(this);
  });  

  $('.additem-btn').on('click',function(e){
    e.preventDefault();
    console.log('wow');

    var data = [];

    data['elem'] = $(this);
    data['type'] = $(this).attr('itemtype');
    FormControl.addpanel(data);

  });

  $(document).on('click','.add-radio-btn', function(e){
    e.preventDefault();
    console.log('click');
    FormControl.addRadio();
  });

  // $(document).on('click', 'add-radio-btn' , function(e){
  //   e.preventDefault();
  //   var elem = $(this);
  //   FormControl.addRadio(elem);
  // });

  $(document).on('click', '.remove-radio-btn', function(e){
    e.preventDefault();
    var elem = $(this);
    FormControl.deleteRadio(elem);
    
  });

  // $('.add-radio-panel-btn').on('click', function(e){
  //   e.preventDefault();
  //   console.log('click');

  //   FormControl.addRadioPanel();
  // });




  //Sample Form Data
  var sampleData = [
    {
      elem: 'input',
      label: 'Age',
      type: 'number',
      required: true
    },

    {
      elem: 'select',
      label: 'Gender',
      options: {
        1: {
          value: 'Male'
        },
        2: {
          value: 'Female'
        }
      }
    },

    {
      elem: 'radio',
      label: 'Salary range',
      options: {
        1: {
          value: '999,999 - 500,000',
          checked: true
        },
        2: {
          value: '499,999 - 100,000',
          checked: false
        }
      },
      required: true
    },

    {
      elem: 'checkbox',
      label: 'Vegetables available',
      options: {
        1: {
          value: 'Carrot',
          checked: true
        },
        2: {
          value: 'Asparagus',
          checked: true
        }
      },
      required: true
    }
  ];

  window.sampleData = sampleData;

  // $document.getElementById("#samplejs").innerHTML = sampleDAta.elem[1] ;

});



