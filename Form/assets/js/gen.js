
$(document).ready(function() {

	var countryList = function(){
		
		var lib = {};

		var countries = ['Japan','Saray'];

		lib.display = function(data){
			$('ul').html('');
				$.each(countries, function(index,value){
					$('ul').append('<li>' + value + '</li>');
			});

		}

		lib.add = function(data){
			countries.push(data);
		}

		lib.del = function(data){
			countries.pop(data);
		}



		return lib;

	}

	var PanelBuilder = function(){

		var foo = {};

		var items = [{value1:"blalala", value2:"asdd"},{value1:"blalala", value2:"asdd"}];

		var html = '<div class="panel panel-default no-round panclass" panel-id="{{id}}" >'+
                   '<div class="panel-body">'+   
                   '<form>'+
                   '<input type="text" class="form-control no-round input-text input-value1" placeholder="firstname">'+
                   '<input type="text" class="form-control no-round input-text input-value2" placeholder="lastname" style="margin-top: 20px">'+
                   '<button id="btn-del-panel" class="btn btn-default no-round">Delete</button>'+
                   '</form>'+  
                   '</div>'+
                   '</div>';


		foo.addPanel = function(data){

			var inputItem = {value1:"", value2:""};

			var index = items.length;
			
	        $('.main').append(html);
	        $('.main .panel:last-child').attr('panel-id',index);

	        items.push(inputItem);
	        console.log(JSON.stringify(items));

		}


		foo.removepanel = function(data){

			var i = 0;
		 	var del = $(data).closest('.panel').attr('panel-id');
		 	$('.panel').remove('[panel-id="'+del+'"]');

			items.splice(del,1);
			
			$('.panclass').each(function(){
				$(this).attr('panel-id', i);
				i++;
			});

		}


		foo.savepanel = function(data){

			$('.input-text').each(function(){
				var id = $(this).closest('.panel').attr('panel-id');
				console.log('[panel-id="'+id+'"]');
				items[id].value1 = $('[panel-id="'+id+'"] .input-text.input-value1').val();
				items[id].value2 = $('[panel-id="'+id+'"] .input-text.input-value2').val();
			});
		}


		foo.generatePanel = function(data){

			var index = items.length;
			
	        $('.main').append(temp);
	        //$('.main .panel:last-child').attr('panel-id',index);

			$.each(items, function(index,value){
				temp = html.replace('{{id}}', 'harold');
				items[id].value1 = $('[panel-id="'+id+'"] .input-text.input-value1').val();
				items[id].value2 = $('[panel-id="'+id+'"] .input-text.input-value2').val();
		
			});
				
		}

        return foo;              
	}



// Instances

	Country = new countryList();

	Panel = new PanelBuilder();

// Event Handlers

	$(document).on('click', '#display-btn', function(e){
		e.preventDefault();
		var inputvalue = $('.inputValue').val();
		
		Country.add(inputvalue);
		Country.display();
		
	});

	$(document).on('click','#del-btn', function(e){
		e.preventDefault();

		Country.del();
		Country.display();
	});
	

	$(document).on('click', '#blue-btn', function(e){
		e.preventDefault();
		$('.box').removeClass('orange-box').addClass('blue-box');

	});

	$(document).on('click', '#orange-btn', function(e){
		e.preventDefault();
		$('.box').removeClass('blue-box').addClass('orange-box');
	});


	$(document).on('click', '#btn-add-panel', function(e){
		e.preventDefault();
		console.log('added');

		Panel.addPanel();
		
	});

	$(document).on('click', '#btn-del-panel', function(e){
		e.preventDefault();
		console.log('deleted');

		Panel.removepanel(this);

	});


	$(document).on('click', '#btn-save-panel', function(e){
		e.preventDefault();
		console.log('saved');

		Panel.savepanel();

	});

	Panel.generatePanel();

});



